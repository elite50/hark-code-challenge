# Hark! Code Challenge

Thanks for your interest in joining the Hark! team. Because we want to bring you onto the team right away, we don't have the luxury of working with you on a contract basis. So to get a sense for your coding style, I've put together this code challenge that shouldn't take more than thirty minutes to an hour.

To take on the challenge, fork the repository - please make your fork private to prevent others copying your solution - finish your code and submit a pull request. In your pull request, include any details that you feel require explanation.

## The Challenge

**Without** using CSS gradients, create a gradient from the outer circle to the inner circle. Here's the beginning of the challenge and a successful completion:

#### Start
![Hark! Challenge Start](http://hark.io/start.png)

#### End
![Hark! Challenge End](http://hark.io/end.png)

## Details

The starting code is barebones and sets the stage. Use any solution you want. This is meant to be a freeform challenge, so do whatever floats your boat.

You're encouraged to reformat the code to your liking and add any extra features you can think of. If you feel your solution is not self-explanatory, or you made some interesting architecture decisions, please include descriptions and reasoning in your pull request or as code comments.

## Thanks

 Thank you for taking the time! I look forward to your solution and hopefully we'll work together in the future.

---

George Yates III -- Project Manager | Technical Lead